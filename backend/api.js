
const express = require('express');
const cors = require('cors');
const PORT = 8081;
const fs = require("fs");

const app = express();

app.use(cors());

app.get('/surprise', function (request, response) {
   fs.readFile( __dirname + "/" + "db.json", 'utf8', function (err, inJson) {
      let outData = JSON.parse(inJson);
      outData.time = (new Date()).toISOString();
      console.log( outData );
      response.end( JSON.stringify(outData) );
   });
})


let server = app.listen(PORT, function () {
   let host = server.address().address
   let port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})